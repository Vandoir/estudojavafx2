package br.com.vf.model.bo;

import java.util.HashMap;
import java.util.Map;

import br.com.vf.model.dto.User;

public class AuthenticatorBo {

    private static final Map<String, User> USERS = new HashMap<String, User>();
    static {
        USERS.put("demo", new User("demo","demo","demo"));
        USERS.put("admin", new User("admin","admin","adm"));
        
    }

    public static boolean validate(String user, String password) {
        User usr = USERS.get(user);
        return usr != null &&  usr.getPassword().equals(password);
    }
	
    public static User getUser(String key) {
        return USERS.get(key);
    }    
    
	
}
