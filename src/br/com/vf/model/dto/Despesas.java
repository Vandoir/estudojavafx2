package br.com.vf.model.dto;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class Despesas {
	
	private final SimpleStringProperty categoria = new SimpleStringProperty("");
	private final SimpleDoubleProperty valor = new SimpleDoubleProperty(0);

	public Despesas() {
		this("", 0);
	}

	public Despesas(String cat, double val) {
		categoria.set(cat);
		valor.set(val);
	}

	public String getCategoria() {
		return categoria.get();
	}

	public void setCategoria(String cat) {
		categoria.set(cat);
	}
	
	public double getValor() {
		return valor.get();
	}

	public void setValor(double val) {
		valor.set(val);
	}

	
	
	
}
