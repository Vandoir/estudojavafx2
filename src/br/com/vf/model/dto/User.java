package br.com.vf.model.dto;

public class User {

    private String user;
    private String password;
    private String rule;
    
	public User(String user, String password, String rule) {
		super();
		this.user = user;
		this.password = password;
		this.rule = rule;
	}
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	
}
