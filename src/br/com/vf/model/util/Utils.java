package br.com.vf.model.util;

import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Utils {
	
	public static Map<String,Long> getFilesForFolder(String rootPath){
		File folder = new File(rootPath);
		Map<String,Long> retorno = new HashMap<String,Long>();
		loadFilesForFolder(folder, folder.getAbsolutePath(), retorno );
		return retorno;
	}
	
	private static void loadFilesForFolder(final File folder, String rootFolder, Map<String,Long> mapa) {
		String key = "";
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				loadFilesForFolder(fileEntry, rootFolder ,mapa);
			} else {
				key = fileEntry.getAbsolutePath(); 
				key = key.replace(rootFolder, "");
				mapa.put(key, fileEntry.lastModified());
			}
		}
	}	

	public static boolean isDirectory(String fileName) {
		File f1 = new File(fileName);
		return f1.isDirectory();
	}

	public static String formatarDouble(double valor){
		
		String retorno = "" ;
		
		DecimalFormat formatNumber = new DecimalFormat("#,##0.00");
	    formatNumber.setGroupingUsed(true);
	    formatNumber.setGroupingSize(3);
	    formatNumber.setParseIntegerOnly(false);
	    
	    retorno = formatNumber.format(valor);
		
		return retorno;
		
	}
	
	public static Date getDate(int pDia, int pMes, int pAno){

		String     ss          = pDia + "/" + pMes+ "/" + pAno;
		SimpleDateFormat dataEntrada = new SimpleDateFormat("dd/MM/yyyy");
		Date       dataRetorno = null;

		try {
			dataRetorno = dataEntrada.parse(ss);
		} catch (ParseException e) {
			e.printStackTrace();
			System.out.println("Data inv�lida: " + ss);
		}
		return dataRetorno;
	}
	

	public static Date parseDMYToDate(String dataTeste) throws Exception{

		Date retorno = null; 
		String[] dmy = dataTeste.split("/");
		int maxDay = 31;
		int dia = 0;
		int mes = 0;
		int ano = 0;

		if (dmy.length==3){
			try {
				dia = Integer.parseInt(dmy[0].trim());
				mes = Integer.parseInt(dmy[1].trim());
				ano = Integer.parseInt(dmy[2].trim());
			} catch (NumberFormatException e) {
				throw new Exception("Caracter invalido : " + e.getMessage() );
			}
			// Valida ano
			if ((ano<1800) || (mes>2100)) {
				throw new Exception("Ano invalido");
			}
			// Valida mes
			if ((mes<1) || (mes>12)) {
				throw new Exception("Mes invalido");
			}
			// Valida dia
			if (mes==2) {
				maxDay=29;
			}
			if (mes==4 || mes==6 || mes==9 || mes==9 ){
				maxDay=30;
			}
			if ((dia<1) || (dia>maxDay)) {	
				throw new Exception("Dia invalido");
			}
			retorno = Utils.getDate(dia, mes, ano);
		} else{
			throw new Exception("Formato da data deve ser DD/MM/AAAA");
		}

		return retorno;
	}    
	
	public static String formatarData(Date data1){
		
		String retorno = "" ;

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		try{
			retorno = sdf.format(data1);
		} catch (Exception e){
			retorno = " ";
		}

		return retorno;
		
	}
	
	
}
