package br.com.vf;

//import impl.org.controlsfx.i18n.Localization;

//import java.util.Locale;
import java.util.Optional;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

//import org.controlsfx.control.action.Action;
//import org.controlsfx.dialog.Dialogs;

import br.com.vf.view.ScreenPane;
import br.com.vf.view.ScreenPaneType;

public class EstudoAppLaunch extends Application {
	
	private Stage primaryStage;
	
	@Override
	public void start(Stage stage) throws Exception {
		
		//Localization.setLocale(new Locale("en")); //Translations.getTranslation("en")
		
		this.primaryStage = stage;
		
		ScreenPane mainContainer = new ScreenPane();
		mainContainer.setPrimaryStage(this.primaryStage);
		mainContainer.loadScreen(ScreenPaneType.LOGIN);
		mainContainer.loadScreen(ScreenPaneType.MENU_METRO);
		mainContainer.loadScreen(ScreenPaneType.DICAS);
		mainContainer.loadScreen(ScreenPaneType.FOLDER_COMPARE);
		mainContainer.loadScreen(ScreenPaneType.FOLDER_REMOVE);
		mainContainer.loadScreen(ScreenPaneType.JM105_DESPESAS);
		mainContainer.loadScreen(ScreenPaneType.JM105_FUNC_LISTA);
		mainContainer.loadScreen(ScreenPaneType.WEB_CAM);
		mainContainer.loadScreen(ScreenPaneType.DB_EXPLORER);
		mainContainer.loadScreen(ScreenPaneType.TEMPLATE);
		mainContainer.setScreen(ScreenPaneType.LOGIN);

		Scene scene = new Scene(mainContainer);
		
		stage.setTitle(String.format(":: %s Vers�o %s", ScreenPane.APP_TITLE, ScreenPane.APP_VERSION));

		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				event.consume(); // Consuming the event by default.
				if (confirmarSaida()){
					System.exit(0);
				}
			}
		}); 
		stage.setScene(scene);
		stage.show();
	}

	private boolean confirmarSaida(){
		//DialogResponse response = Dialogs.showConfirmDialog(primaryStage,"Deseja sair do sistema?", ScreenPane.DLG_CONFIRM, ScreenPane.APP_TITLE , DialogOptions.YES_NO);
		//return (response == DialogResponse.YES);
		
		
		
		//Action response = Dialogs.create().owner(primaryStage).title(ScreenPane.APP_TITLE).masthead(ScreenPane.DLG_CONFIRM).message("Deseja sair do sistema?").actions(org.controlsfx.dialog.Dialog.Actions.YES, org.controlsfx.dialog.Dialog.Actions.NO).showConfirm();
		//return (response == org.controlsfx.dialog.Dialog.Actions.YES );
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(ScreenPane.APP_TITLE);
		alert.setHeaderText(ScreenPane.DLG_CONFIRM);
		alert.setContentText("Deseja sair do sistema?");

		Optional<ButtonType> result = alert.showAndWait();
		
		return (result.get() == ButtonType.OK);
		
		
	}


	/**
	 * The main() method is ignored in correctly deployed JavaFX application.
	 * main() serves only as fallback in case the application can not be
	 * launched through deployment artifacts, e.g., in IDEs with limited FX
	 * support. NetBeans ignores main().
	 *
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}


}
