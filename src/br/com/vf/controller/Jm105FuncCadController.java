package br.com.vf.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import br.com.vf.model.dto.Funcionario;
import br.com.vf.view.ScreenPane;

public class Jm105FuncCadController implements Initializable {

	private Stage dialogStage;
	
	private boolean okClicked = false;
	
	private Funcionario funcionario = null;
	
	@FXML
	private TextField tfCodigo;
	
	@FXML
	private TextField tfNome;
	
	@FXML
	private TextField tfCargo;

	@FXML
	private TextField tfSalario;
	
	public boolean isOkClicked() {
		return okClicked;
	}
	
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}
	
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
		this.entityToScreen();
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		
	}
	
	@FXML
	private void handleActionSave(ActionEvent event) {
		if (isInputValid()){
			this.screenToEntity();
			okClicked = true;
			dialogStage.close();
		}
	}

	@FXML
	private void handleActionCancel(ActionEvent event) {
		dialogStage.close();
	}
	
	private boolean isInputValid() {
		String errorMessage = "";

		if (tfCodigo.getText() == null || tfCodigo.getText().length() == 0) {
			errorMessage += "C�digio � um campo obrigat�rio!\n"; 
		} else {
			try {
				int vlrCodInput = Integer.parseInt(tfCodigo.getText());
				if (!(vlrCodInput>0)){
					errorMessage += "C�digo deve ser maior que zero!\n";
				}
			} catch (Exception e) {
				errorMessage += e.getMessage();
			}
		}
		
		if (tfNome.getText() == null || tfNome.getText().length() == 0) {
			errorMessage += "Nome � um campo obrigat�rio!\n"; 
		}

		if (tfCargo.getText() == null || tfCargo.getText().length() == 0) {
			errorMessage += "Cargo � um campo obrigat�rio!\n"; 
		}
		
		if (tfSalario.getText() == null || tfSalario.getText().length() == 0) {
			errorMessage += "Salario � um campo obrigat�rio!\n"; 
		} else {
			try {
				double vlrSalInput = Double.parseDouble(tfSalario.getText());
				if (vlrSalInput<0.0){
					errorMessage += "Salario n�o pode ser menor que zero!\n";
				}
			} catch (Exception e) {
				errorMessage += e.getMessage();
			}
		}
	
		
		if (errorMessage.length() == 0) {
			return true;
		} else {
			// Show the error message
		
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle(ScreenPane.DLG_INVALID_FIELD_TITLE);
			alert.setHeaderText(ScreenPane.DLG_INVALID_FIELD_HEAD);
			alert.setContentText(errorMessage);
			alert.showAndWait(); 

			
			//org.controlsfx.dialog.Dialogs.create().owner(this.dialogStage).title(ScreenPane.DLG_INVALID_FIELD_TITLE).message(errorMessage).masthead(ScreenPane.DLG_INVALID_FIELD_HEAD).showError();
			return false;
		}
	}
	
	private void entityToScreen(){
		tfCodigo.setText(String.valueOf(funcionario.getCodigo()));
		tfNome.setText(funcionario.getNome());
		tfCargo.setText(funcionario.getCargo());
		tfSalario.setText(String.valueOf(funcionario.getSalario()));
	}
	
	private void screenToEntity(){
		funcionario.setCodigo(Integer.parseInt(tfCodigo.getText()));
		funcionario.setNome(tfNome.getText());
		funcionario.setCargo(tfCargo.getText());
		funcionario.setSalario(Double.parseDouble(tfSalario.getText()));
	}

	
}
