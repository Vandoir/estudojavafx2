package br.com.vf.controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ConexaoJdbcController implements Initializable {
	
	public static final String DB_FIREBIRD  = "Firebird";
	public static final String DB_CACHE     = "Cache";
	public static final String DB_MYSQL     = "MySql";
	public static final String DB_ORACLE    = "Oracle";
	public static final String DB_DB2       = "DB2";
	public static final String DB_SQLSERVER = "SqlServer";
	public static final String DB_SQLITE    = "SQLite";

	@FXML
	private ComboBox<String> cbxDriver;
	@FXML
	private TextField tfHost;
	@FXML
	private TextField tfPorta;
	@FXML
	private TextField tfDatabaseFile;
	@FXML
	private TextField tfUserName;
	@FXML
	private PasswordField pfPassword;
	
	private Stage dialogStage;
	private boolean okClicked = false;
	
	public boolean isOkClicked() {
		return okClicked;
	}
	
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}




	@Override
	public void initialize(URL url, ResourceBundle rb) {

		cbxDriver.getItems().setAll(ConexaoJdbcController.DB_FIREBIRD,
				ConexaoJdbcController.DB_CACHE,
				ConexaoJdbcController.DB_MYSQL,
				ConexaoJdbcController.DB_ORACLE,
				ConexaoJdbcController.DB_DB2,
				ConexaoJdbcController.DB_SQLSERVER,
				ConexaoJdbcController.DB_SQLITE);

		cbxDriver.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> selected, String oldOpc, String newOpc) {

				if (newOpc.equals(ConexaoJdbcController.DB_CACHE)){
					tfPorta.setText("1972");
				} else if (newOpc.equals(ConexaoJdbcController.DB_MYSQL)){
					tfPorta.setText("3306");
				} else if (newOpc.equals(ConexaoJdbcController.DB_ORACLE)){
					tfPorta.setText("1521");
				} else if (newOpc.equals(ConexaoJdbcController.DB_SQLITE)){
					tfPorta.setText("0");
				} else if (newOpc.equals(ConexaoJdbcController.DB_DB2)){
					tfPorta.setText("50000");
				} else if (newOpc.equals(ConexaoJdbcController.DB_SQLSERVER)){
					tfPorta.setText("1433");
				} else {
					tfPorta.setText("3050");
				}
			}
		});	

		cbxDriver.getSelectionModel().selectFirst();

		tfHost.setText("localhost");
		tfPorta.setText("3050");
		tfDatabaseFile.setText("ss");
		tfUserName.setText("SYSDBA");

	}

	@FXML
	private void handleActionConectar(ActionEvent event) {
		if (conectou()){
			okClicked = true;
			dialogStage.close();
		}
	}
	
	private boolean conectou(){
		
		System.out.println(tfPorta.getText());
		System.out.println(tfDatabaseFile.getText());
		System.out.println(tfUserName.getText());
		System.out.println(pfPassword.getText());
		
		return true; 
	}

	@FXML
	private void handleActionFindDB(ActionEvent event) {
		
	      FileChooser fileChooser = new FileChooser();

	      // Set extension filter
	      FileChooser.ExtensionFilter extFilterfdb = new FileChooser.ExtensionFilter(
	              "FDB files (*.fdb)", "*.fdb");
	      FileChooser.ExtensionFilter extFilterlite = new FileChooser.ExtensionFilter(
	              "Sqlite files", new String[] {"*.db", "*.db3"});
	      
	      fileChooser.getExtensionFilters().add(extFilterfdb);
	      fileChooser.getExtensionFilters().add(extFilterlite);

	      // Show save file dialog
	      File file = fileChooser.showOpenDialog(null);
	      
	      if (file != null) {
	    	  tfDatabaseFile.setText(file.getAbsolutePath());
	      }	
		
	}
	
	

}
