package br.com.vf.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.web.WebView;
import br.com.vf.view.ScreenPane;
import br.com.vf.view.ScreenPaneType;

public class DbExplorerController extends BaseController implements Initializable {

	@FXML
	private TextArea taCmd;
	
	@FXML
	private TextArea taCsv;

	@FXML
	private WebView wvResult;
	
	@FXML
	private ComboBox<String> cbxOpcao;

	@FXML
	private TabPane tpResult;
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		
		cbxOpcao.getItems().setAll("Grid", "CSV");
		
		cbxOpcao.getSelectionModel().getSelectedItem(); 
		
		cbxOpcao.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> selected, String oldOpc, String newOpc) {
				System.out.println("old =" + oldOpc+ ", new  =" + newOpc);
			}
		});
		
		SingleSelectionModel<Tab> selectionModel = tpResult.getSelectionModel();
		//selectionModel.select(tab); //select by object
		selectionModel.select(0); //select by index starting with 0
		//selectionModel.clearSelection(); //clear your selection
		
	}

	@FXML
	private void handleActionBackMenuMetro(ActionEvent event) {
		myScreenPane.setScreen(ScreenPaneType.MENU_METRO);
	}
	
	@FXML
	private void handleActionRun(ActionEvent event) {
		
		System.out.println("comob="+ cbxOpcao.getSelectionModel().getSelectedItem());
		
		Connection connection = null; 
		try {
			connection = null; // VfConnection.getInstance().getConnection();
			
			if ("CSV".equals(cbxOpcao.getSelectionModel().getSelectedItem())){
				taCsv.setText( this.buildCSV(connection, taCmd.getText() ) );
			} else {
				wvResult.getEngine().loadContent(this.buildHTML(connection, taCmd.getText() ) );
			}
		} catch (Exception e) {
			e.printStackTrace();
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle(ScreenPane.APP_TITLE);
			alert.setHeaderText(ScreenPane.DLG_ERROR_EXCEPTION);
			alert.setContentText(e.getMessage());
			alert.showAndWait(); 
			//org.controlsfx.dialog.Dialogs.create().owner(myScreenPane.getPrimaryStage()).title(ScreenPane.APP_TITLE).message(e.getMessage()).masthead(ScreenPane.DLG_ERROR_EXCEPTION).showException(e);
		} finally {
		        
			if (connection!=null){
				try {
					//VfConnection.close();
					System.out.println("fechou");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}
	

	private String buildHTML(Connection connection, String cmd) throws Exception {
		
		StringBuilder sb = new StringBuilder ();
		
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			// Prepara a execu��o do select
			ps = connection.prepareStatement(cmd);
			// Executa a consulta
			rs = ps.executeQuery();
			// Salva os campos no Array de Retorno
			
			sb.append("<head>");
			sb.append("<style>");
			sb.append("table");
			sb.append("{");
			sb.append("border-collapse:collapse;");
			sb.append("}");
			sb.append("table, td, th");
			sb.append("{");
			sb.append("border:1px solid black  ;");
			sb.append("}");
			sb.append("th");
			sb.append("{");
			sb.append("background-color: #4C4C33 ;");
			sb.append("color:white;");
			sb.append("}");
			sb.append("td , th");
			sb.append("{");
			sb.append("padding: 2px 5px 2px 5px;"); //1-cima, 2-direita, 3-baixo, 4-esquerda
			sb.append("}");
			sb.append("tr:nth-child(even)");
			sb.append("{");
			sb.append("background-color:#E0E0D1;");
			sb.append("}");
			sb.append("</style>");
			sb.append("</head>");

			sb.append("<table border='1' cellspacing='0' cellpadding='0'>");
			
			ResultSetMetaData metaData = rs.getMetaData() ;
			
			int columnCount = metaData.getColumnCount() ;
			
			sb.append("<tr>");
			sb.append(" <th>RECNO</th>");
			for (int i = 1; i <= columnCount ; i++) {
				sb.append(" <th>"+metaData.getColumnName(i) + " ["+  metaData.getColumnTypeName(i) + " -> " + metaData.getColumnType(i) +"] </th>");
			}			
			sb.append("</tr>");
			
			String vlrFormat = "";
			int recno = 0; 
			
			while (rs.next()) {
				recno++;
				sb.append("<tr class='alt'>");
				sb.append(" <td>"+recno+"</td>");
				for (int i = 1; i <= columnCount ; i++) {
					if (metaData.getColumnType(i) == -4){
						vlrFormat = "blob";
					} else {
					vlrFormat = rs.getString(i);
					}
					/*
					if (metaData.getColumnType(i) == 1 ||
						metaData.getColumnType(i) == -1 ||
						metaData.getColumnType(i) == 12 ){
						
						valorFormatado = "\'" + resultSet.getString(i)+ "\'";
					} else if (metaData.getColumnType(i) == 91 ) {
						valorFormatado = formatarDate(resultSet.getDate(i));
					} else {
						valorFormatado = resultSet.getString(i);
					}
					 */
					sb.append(" <td>"+vlrFormat+"</td>");
				}
				sb.append("</tr>");
			}
			
			sb.append("</table>");
		} catch (SQLException e) {
			throw new Exception(e);
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			try {
				if (rs!=null) rs.close() ;
				if (ps!=null) ps.close() ;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		sb.append("<br/><br/><br/>");
		return sb.toString();
		
	}
	
	private String buildCSV(Connection connection, String cmd) throws Exception {
		
		StringBuilder sb = new StringBuilder ();
		
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			// Prepara a execu��o do select
			ps = connection.prepareStatement(cmd);
			// Executa a consulta
			rs = ps.executeQuery();
			// Salva os campos no Array de Retorno
			
			ResultSetMetaData metaData = rs.getMetaData() ;
			
			int columnCount = metaData.getColumnCount() ;
			
			sb.append("RECNO");
			for (int i = 1; i <= columnCount ; i++) {
				sb.append(";"+metaData.getColumnName(i));
			}			
			sb.append("\n");
			
			String vlrFormat = "";
			int recno = 0; 
			
			while (rs.next()) {
				recno++;
				sb.append(recno);
				for (int i = 1; i <= columnCount ; i++) {
					if (metaData.getColumnType(i) == -4){
						vlrFormat = "blob";
					} else {
					vlrFormat = rs.getString(i);
					}
					/*
					if (metaData.getColumnType(i) == 1 ||
						metaData.getColumnType(i) == -1 ||
						metaData.getColumnType(i) == 12 ){
						
						valorFormatado = "\'" + resultSet.getString(i)+ "\'";
					} else if (metaData.getColumnType(i) == 91 ) {
						valorFormatado = formatarDate(resultSet.getDate(i));
					} else {
						valorFormatado = resultSet.getString(i);
					}
					 */
					sb.append(";"+vlrFormat);
				}
				sb.append("\n");
			}
			
		} catch (SQLException e) {
			throw new Exception(e);
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			try {
				if (rs!=null) rs.close() ;
				if (ps!=null) ps.close() ;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		sb.append("\n\n\n");
		return sb.toString();
		
	}
	


}
