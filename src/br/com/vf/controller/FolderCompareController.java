package br.com.vf.controller;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import br.com.vf.model.util.Utils;
import br.com.vf.view.ScreenPane;
import br.com.vf.view.ScreenPaneType;

public class FolderCompareController extends BaseController implements Initializable {

	@FXML
	private TextField tfPasta1;

	@FXML
	private TextField tfPasta2;
	
	@FXML
	private TextField tfExtensoes;

	@FXML
	private TextArea taResult;
	
	@FXML
	private WebView wvResult;
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		tfPasta1.setText("D:\\Desenv");
		tfPasta2.setText("D:\\Repository");
		//WebEngine webEngine = wvDicas.getEngine();
		//webEngine.load("http://www.javadesk.co/css/javaFXCssIntro.html");
		//webEngine.loadContent("<h1>um</h1><h2>dois</h2>");
		//webEngine.load(getClass().getResource("../view/Dicas.html").toExternalForm());
	}

	@FXML
	private void handleActionBackMenuMetro(ActionEvent event) {
		myScreenPane.setScreen(ScreenPaneType.MENU_METRO);
	}
	
	@FXML
	private void handleActionBtnPasta1(ActionEvent event) {

		DirectoryChooser directoryChooser = new DirectoryChooser();
		File selectedDirectory = directoryChooser.showDialog(null);

		if(selectedDirectory != null){
			tfPasta1.setText(selectedDirectory.getAbsolutePath());
		}	
	}

	@FXML
	private void handleActionBtnPasta2(ActionEvent event) {
		DirectoryChooser directoryChooser = new DirectoryChooser();
		File selectedDirectory = directoryChooser.showDialog(null);

		if(selectedDirectory != null){
			tfPasta2.setText(selectedDirectory.getAbsolutePath());
		}	
	}
	
	@FXML
	private void handleActionBtnComparar(ActionEvent event) {
		if (isInputValid()){
			lendoPastas();
		}
	}
	
	private boolean isInputValid() {
		String errorMessage = "";
		
		if (tfPasta1.getText() == null || tfPasta1.getText().length() == 0) {
			errorMessage += "Pasta 1 � um campo obrigat�rio!\n"; 
		} else {
			if (!Utils.isDirectory(tfPasta1.getText())){
				errorMessage += "Pasta 1 n�o � um diret�rio v�lido!\n";
			}
		}

		if (tfPasta2.getText() == null || tfPasta2.getText().length() == 0) {
			errorMessage += "Pasta 2 � um campo obrigat�rio!\n"; 
		} else {
			if (!Utils.isDirectory(tfPasta2.getText())){
				errorMessage += "Pasta 2 n�o � um diret�rio v�lido!\n";
			}
		}
		
		if (errorMessage.length() == 0) {
			return true;
		} else {
			// Show the error message
			
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle(ScreenPane.DLG_INVALID_FIELD_TITLE);
			alert.setHeaderText(ScreenPane.DLG_INVALID_FIELD_HEAD);
			alert.setContentText(errorMessage);
			alert.showAndWait(); 

			
			//org.controlsfx.dialog.Dialogs.create().owner(myScreenPane.getPrimaryStage()).title(ScreenPane.DLG_INVALID_FIELD_TITLE).message(errorMessage).masthead(ScreenPane.DLG_INVALID_FIELD_HEAD).showError();
			return false;
		}
	}
	
	private void lendoPastas(){
		
		List<CompareFolderVo> lista = new ArrayList<CompareFolderVo>();
		
		CompareFolderVo compVo = null; 
		
		Map<String,Long> mapPasta1 = Utils.getFilesForFolder(tfPasta1.getText());
		
		Iterator<Entry<String, Long>> iter1 = mapPasta1.entrySet().iterator();
		 
		while (iter1.hasNext()) {
			Map.Entry<String, Long> mEntry = (Map.Entry<String, Long>) iter1.next();
			System.out.println(mEntry.getKey() + " : " + mEntry.getValue());
			lista.add(new CompareFolderVo(mEntry.getKey(), mEntry.getValue()));
		}
		
		Map<String,Long> mapPasta2 = Utils.getFilesForFolder(tfPasta2.getText());
		
		Iterator<Entry<String, Long>> iter2 = mapPasta2.entrySet().iterator();
		 
		while (iter2.hasNext()) {
			Map.Entry<String, Long> mEntry = (Map.Entry<String, Long>) iter2.next();
			
			compVo = filtrarFile(mEntry.getKey(), lista);
			if (compVo!=null){
				compVo.setData2(mEntry.getValue());
			} else {
				compVo = new CompareFolderVo(mEntry.getKey(),0);
				compVo.setData2(mEntry.getValue());
				lista.add(compVo);
			}
		}
		
		Collections.sort(lista);
		
		// Gerando o CSV
		StringBuilder sbCsv = new StringBuilder();
		sbCsv.append("*;Pasta 1 : "+tfPasta1.getText() + "\n");
		sbCsv.append("*;Pasta 2 : "+tfPasta2.getText() + "\n");
		sbCsv.append("Status;Arquivo;Pasta 1;Pasta 2\n");
		long difTempo = 0;
		
		for (CompareFolderVo compareVo : lista) {
			if (!compareVo.getFile().endsWith(tfExtensoes.getText())){
				difTempo = compareVo.getData2() - compareVo.getData1();
				sbCsv.append(compareVo.getStatus() + ";" + compareVo.getFile() + ";" + formatarData(new Date(compareVo.getData1())) +  ";"  + formatarData(new Date(compareVo.getData2()))+";"+difTempo + "\n" ) ;
			}
		}
		
		taResult.setText(sbCsv.toString());
		
		// Gerando o Html
		
		StringBuilder sb = new StringBuilder();
		
		int tot2n = 0;
		int tot1n = 0;
		int tot0  = 0;
		int tot1p = 0;
		int tot2p = 0;
		
		sb.append("<html>");
		sb.append("<head>");
		sb.append("<style>");
		sb.append("table");
		sb.append("{");
		sb.append("border-collapse:collapse;");
		sb.append("}");
		sb.append("table, td, th");
		sb.append("{");
		sb.append("border:1px solid black  ;");
		sb.append("}");
		sb.append("th");
		sb.append("{");
		sb.append("background-color: #4C4C33 ;");
		sb.append("color:white;");
		sb.append("}");
		sb.append("td , th");
		sb.append("{");
		sb.append("padding: 2px 5px 2px 5px;"); 
		sb.append("}");
		sb.append(".class2n {background-color: #0099CC ;}");
		sb.append(".class1n {background-color: #99E6E6 ;}");
		sb.append(".class0  {background-color: #D6D6C2 ;}");
		sb.append(".class1p {background-color: #FFC266 ;}");
		sb.append(".class2p {background-color: #FF9900 ;}");
		sb.append("</style>");
		sb.append("</head>");
		sb.append("<table border='1' cellspacing='0' cellpadding='0'>");
		sb.append("<tr>");
		sb.append("<th>Status</th>");
		sb.append("<th>Arquivo</th>");
		sb.append("<th>Pasta 1</th>");
		sb.append("<th>Pasta 2</th>");
		sb.append("<th>Diferenca</th>");
		sb.append("</tr>");
		
		int status = 0;
		
		for (CompareFolderVo compareVo : lista) {
			if (!compareVo.getFile().endsWith(tfExtensoes.getText())){
				difTempo = compareVo.getData2() - compareVo.getData1();
				status = compareVo.getStatus();
				switch (status) {
				case -2:
					sb.append("<tr class='class2n'>"); tot2n++; break;
				case -1:
					sb.append("<tr class='class1n'>"); tot1n++; break;
				case 1:
					sb.append("<tr class='class1p'>"); tot1p++; break;
				case 2:
					sb.append("<tr class='class2p'>"); tot2p++; break;
				default:
					sb.append("<tr class='class0'>"); tot0++; break;
				}
				sb.append(" <td>"+status+"</td>");
				sb.append(" <td>"+compareVo.getFile()+"</td>");
				sb.append(" <td>"+formatarData(new Date(compareVo.getData1()))+"</td>");
				sb.append(" <td>"+formatarData(new Date(compareVo.getData2()))+"</td>");
				sb.append(" <td>"+difTempo+"</td>");
				sb.append("</tr>");
			}
		}
		sb.append("</table>");
		sb.append("<h1>Resumo</h1>");
		sb.append("<table>");
		sb.append("<tr class='class2n'> <td>-2</td> <td>Nao encontrado na pasta 2</td> <td>"+tot2n+"</td> </tr>");
		sb.append("<tr class='class1n'> <td>-1</td> <td>Nao encontrado na pasta 1</td> <td>"+tot1n+"</td> </tr>");
		sb.append("<tr class='class0'>  <td> 0</td> <td>Iguais</td> <td>"+tot0+"</td> </tr>");
		sb.append("<tr class='class1p'> <td> 1</td> <td>Ultima alteracao na pasta 1</td> <td>"+tot1p+"</td> </tr>");
		sb.append("<tr class='class2p'> <td> 2</td> <td>Ultima alteracao na pasta 2</td> <td>"+tot2p+"</td> </tr>");
		sb.append("</table>");
		sb.append("</html>");
		
		wvResult.getEngine().loadContent(sb.toString());
		return;

	}
	
	private CompareFolderVo filtrarFile(String file, List<CompareFolderVo> listaCompare ){
		
		for (CompareFolderVo compareVo : listaCompare) {
			if (compareVo.getFile().equals(file)){
				return compareVo;
			}
		}
		return null;
	}
	
	private String formatarData(Date data1){
		String retorno = " " ;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try{
			if (data1.getTime()!=0){
				retorno = sdf.format(data1);
			}
		} catch (Exception e){
			retorno = " ";
		}
		return retorno;
	}
	
	class CompareFolderVo implements Comparable<CompareFolderVo> {
		
		private String file = "";
		private long data1 = 0;
		private long data2 = 0;
		
		public CompareFolderVo(String file) {
			super();
			this.file = file;
		}
		
		public CompareFolderVo(String file, long data1) {
			super();
			this.file = file;
			this.data1 = data1;
		}

		public long getData1() {
			return data1;
		}

		public void setData1(long data1) {
			this.data1 = data1;
		}

		public long getData2() {
			return data2;
		}

		public void setData2(long data2) {
			this.data2 = data2;
		}

		public String getFile() {
			return file;
		}
		
		public int getStatus(){
			int retorno = 0 ;
			if (data1!=0 && data2!=0){
				if (data1==data2){
					retorno = 0 ;
				} else {
					if (data1>data2){
						retorno = 1 ;
					} else {
						retorno = 2 ;
					}
				}
			} else {
				if (data1!=0) {
					retorno = -2;
				} else {
					retorno = -1;
				}
			}
			return retorno;
		}

		@Override
		public int compareTo(CompareFolderVo o) {
			return file.compareTo(o.getFile());
		}
    }
	
}
