package br.com.vf.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import br.com.vf.view.ScreenPane;
import br.com.vf.view.ScreenPaneType;

/*
import com.googlecode.javacpp.Loader;
import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.OpenCVFrameGrabber;
import com.googlecode.javacv.cpp.opencv_objdetect;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import static com.googlecode.javacv.cpp.opencv_objdetect.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;
*/
public class WebCamController extends BaseController implements Initializable {
	
	@FXML
	private Button initSystemButton;
	@FXML
	private Button startCaptureButton;
	@FXML
	private Button stopCaptureButton;
	@FXML
	private Button shutdownSystemButton;
	@FXML
	private Button stressTestButton;
	@FXML
	private Button takeSnapshotButton;

	@FXML
	private TextArea logTextArea;

	@FXML
	private Label lbXX;

	@FXML
	private Label messageLabel;
	
	@FXML
	private TextField tfXX;
	
	@FXML
	private void handleActionBackMenuMetro(ActionEvent event) {
		myScreenPane.setScreen(ScreenPaneType.MENU_METRO);
	}

	@FXML
	private void handleActionBtnInitSystem(ActionEvent event) {
		logTextArea.setText("handleActionBtnInitSystem");
	}

	@FXML
	private void handleActionBtnStartCapture(ActionEvent event) {
		logTextArea.setText("handleActionBtnStartCapture");
	}
	
	@FXML
	private void handleActionBtnStopCapture(ActionEvent event) {
		logTextArea.setText("handleActionBtnStopCapture");
	}
	
	@FXML
	private void handleActionBtnShutdownSystem(ActionEvent event) {
		logTextArea.setText("handleActionBtnShutdownSystem");
	}
	
	@FXML
	private void handleActionBtnTakeSnapshot(ActionEvent event) {
		captureSnap();
		
	}
	
	
	@FXML
	private void handleActionBtnstressTest(ActionEvent event) {
		logTextArea.setText("handleActionBtnstressTest");
	}
	
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// initialize
	}
	
	@FXML
	private void onClickLabelXX(MouseEvent event) {
		if (event.getClickCount()==2){
			tfXX.setText("onClick");
		}
	}

	@FXML
	private void handleActionOk(ActionEvent event) {
		if (isInputValid()){
			tfXX.setText("Ok");
		}
	}
	
	private boolean isInputValid() {
		String errorMessage = "";
		
		if (tfXX.getText() == null || tfXX.getText().length() == 0) {
			errorMessage += "Pasta 1 � um campo obrigat�rio!\n"; 
		}

		if (tfXX.getText() == null || tfXX.getText().length() == 0) {
			errorMessage += "Pasta 2 � um campo obrigat�rio!\n"; 
		}
		
		if (errorMessage.length() == 0) {
			return true;
		} else {
			// Show the error message
			
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle(ScreenPane.DLG_INVALID_FIELD_TITLE);
			alert.setHeaderText(ScreenPane.DLG_INVALID_FIELD_HEAD);
			alert.setContentText(errorMessage);
			alert.showAndWait(); 

			
			//org.controlsfx.dialog.Dialogs.create().owner(myScreenPane.getPrimaryStage()).title(ScreenPane.DLG_INVALID_FIELD_TITLE).message(errorMessage).masthead(ScreenPane.DLG_INVALID_FIELD_HEAD).showError();
			return false;
		}
	}
	
	private void captureSnap(){
		
		System.out.println("java.library.path="+System.getProperty("java.library.path"));
		logTextArea.setText("handleActionBtnTakeSnapshot");	
		/*
		final OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
        try {
            grabber.start();
            IplImage img = grabber.grab();
            if (img != null) {

            	BufferedImage img1 = img.getBufferedImage();

                ImageIcon icon = new ImageIcon(img1);
                JLabel label = new JLabel(icon);
                JOptionPane.showMessageDialog(null, label);
            	
            	//File outputfile = new File("c:\\logs\\capturebi.jpg");
            	//ImageIO.write(img1, "jpg", outputfile);
            	
                cvSaveImage("c:\\logs\\capture.jpg", img);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }		
        */
	}
	

}
