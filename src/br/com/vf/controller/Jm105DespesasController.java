package br.com.vf.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import br.com.vf.model.dto.Despesas;
import br.com.vf.model.util.Utils;
import br.com.vf.view.ScreenPane;
import br.com.vf.view.ScreenPaneType;

public class Jm105DespesasController extends BaseController implements Initializable {
	
	private ObservableList<PieChart.Data> listaDeDespesas = FXCollections.observableArrayList();

	@FXML
	private Label lbXX;
	
	@FXML
	private TextField tfCategoria;

	@FXML
	private TextField tfValor;

	@FXML
	private TableView<Despesas> tabDespesas;
	
	@FXML
	private TableColumn<Despesas, String> colCategoria;
	
	@FXML
	private TableColumn<Despesas, Double> colValor;
	
	@FXML
	private PieChart chartDespesas;

	@FXML
	private void handleActionBackMenuMetro(ActionEvent event) {
		myScreenPane.setScreen(ScreenPaneType.MENU_METRO);
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		
		tfCategoria.setText("IPVA");
		tfValor.setText("627");
		
		this.populate();
		colCategoria.setCellValueFactory(new PropertyValueFactory<Despesas, String>("categoria"));
		colValor.setCellValueFactory(new PropertyValueFactory<Despesas, Double>("valor"));
		chartDespesas.setData(listaDeDespesas);
		
		colValor.setCellFactory(new Callback<TableColumn<Despesas,Double>, TableCell<Despesas,Double>>() {
			@Override
			public TableCell<Despesas, Double> call(TableColumn<Despesas, Double> param) {
				TableCell<Despesas, Double> cell = new TableCell<Despesas, Double>(){
					@Override
					protected void updateItem(Double item, boolean empty) {
						if (item == getItem()) {
		                    return;
		                }
		                super.updateItem( item, empty);
		                if (item == null) {
		                    super.setText(null);
		                    super.setGraphic(null);
		                } else {
		                    super.setText(Utils.formatarDouble(item));
		                    super.setGraphic(null);
		                }	
					}
				};
				//cell.setTextAlignment(TextAlignment.RIGHT);
				cell.setAlignment(Pos.CENTER_RIGHT);
				return cell;
			
			}
		});
		
		colCategoria.setCellFactory(new Callback<TableColumn<Despesas,String>, TableCell<Despesas,String>>() {
			@Override
			public TableCell<Despesas, String> call(TableColumn<Despesas, String> param) {
				TableCell<Despesas, String> cell = new TableCell<Despesas, String>(){
					@Override
					protected void updateItem(String item, boolean empty) {
						if (item == getItem()) {
		                    return;
		                }
		                super.updateItem( item, empty);
		                if (item == null) {
		                    super.setText(null);
		                    super.setGraphic(null);
		                } else {
		                    super.setText(item.toString());
		                    super.setGraphic(null);
		                }	
					}
				};
				//cell.setTextAlignment(TextAlignment.RIGHT);
				cell.setAlignment(Pos.CENTER_LEFT);
				return cell;
			}
		});
		
		// initialize
	}
	
	@FXML
	private void handleActionOk(ActionEvent event) {
		if (isInputValid()){
			Despesas despesa = this.screenToEntity();
			listaDeDespesas.add( new PieChart.Data(despesa.getCategoria(), despesa.getValor()));
			tabDespesas.getItems().add(despesa);
			
			clearScreen();
		}
	}
	
	
	private void populate(){
		Despesas agua = new Despesas("Agua", 20.6);
		listaDeDespesas.add( new PieChart.Data(agua.getCategoria(), agua.getValor()));
		tabDespesas.getItems().add(agua);

		Despesas luz = new Despesas("Luz", 105);
		listaDeDespesas.add( new PieChart.Data(luz.getCategoria(), luz.getValor()));
		tabDespesas.getItems().add(luz);

		Despesas telefone = new Despesas("Telefone", 39);
		listaDeDespesas.add( new PieChart.Data(telefone.getCategoria(), telefone.getValor()));
		tabDespesas.getItems().add(telefone);
		
		Despesas internet = new Despesas("Internet", 45);
		listaDeDespesas.add( new PieChart.Data(internet.getCategoria(), internet.getValor()));
		tabDespesas.getItems().add(internet);
		
		Despesas iptu = new Despesas("IPTU", 255);
		listaDeDespesas.add( new PieChart.Data(iptu.getCategoria(), iptu.getValor()));
		tabDespesas.getItems().add(iptu);
		
	}
	
	private void clearScreen(){
		tfCategoria.setText("");
		tfValor.setText("");
		
	}
	
	private Despesas screenToEntity(){
		Despesas retorno = new Despesas();
		retorno.setCategoria(tfCategoria.getText());
		retorno.setValor(Double.parseDouble(tfValor.getText()));
		return retorno;
	}
	
	private boolean isInputValid() {
		String errorMessage = "";
		
		if (tfCategoria.getText() == null || tfCategoria.getText().length() == 0) {
			errorMessage += "Categoria � um campo obrigat�rio!\n"; 
		}

		if (tfValor.getText() == null || tfValor.getText().length() == 0) {
			errorMessage += "Valor � um campo obrigat�rio!\n"; 
		} else {
			try {
				double vlrInput = Double.parseDouble(tfValor.getText());
				if (!(vlrInput>0.0)){
					errorMessage += "Valor deve ser maior que zero!\n";
				}
			} catch (Exception e) {
				errorMessage += e.getMessage();
			}
		}
		if (errorMessage.length() == 0) {
			return true;
		} else {
			// Show the error message
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle(ScreenPane.DLG_INVALID_FIELD_TITLE);
			alert.setHeaderText(ScreenPane.DLG_INVALID_FIELD_HEAD);
			alert.setContentText(errorMessage);
			alert.showAndWait(); 

			//org.controlsfx.dialog.Dialogs.create().owner(myScreenPane.getPrimaryStage()).title(ScreenPane.DLG_INVALID_FIELD_TITLE).message(errorMessage).masthead(ScreenPane.DLG_INVALID_FIELD_HEAD).showError();
			return false;
		}
	}


	
}
