package br.com.vf.controller;

import br.com.vf.view.ScreenController;
import br.com.vf.view.ScreenPane;

public class BaseController implements ScreenController{
	
	protected ScreenPane myScreenPane;

	@Override
	public void setScreenPane(ScreenPane screenPage) {
		this.myScreenPane = screenPage;
	}

	@Override
	public void refreshScreen() {
		// TODO Auto-generated method stub
	}

}
