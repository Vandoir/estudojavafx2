package br.com.vf.controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import br.com.vf.view.ScreenPaneType;

public class FolderRemoveController extends BaseController implements Initializable {
	
	private StringBuilder sb = new StringBuilder();

	@FXML
	private TextField tfRoot;
	
	@FXML
	private TextField tfName;
	
	@FXML
	private TextArea taResult;
	
	@FXML
	private void handleActionBackMenuMetro(ActionEvent event) {
		myScreenPane.setScreen(ScreenPaneType.MENU_METRO);
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		
		tfRoot.setText("D:\\Repository");
		//
	}
	

	@FXML
	private void aaa(ActionEvent event) {
		
		DirectoryChooser directoryChooser = new DirectoryChooser(); 
        //directoryChooser.setTitle("This is my file ch");
        File file = directoryChooser.showDialog(null);
       if(file!=null){
    	   tfRoot.setText(file.getPath());
       }		
		
	}
	
	@FXML
	private void bbb(ActionEvent event) {
		if (isInputValid()){
			remover();
		}
		
	}
	
	private boolean isInputValid(){
		
		System.out.println("tfRoot = " + tfRoot.getText());
		System.out.println("tfName = " + tfName.getText());
		
		return true;
	}
	
	private void remover(){
		
		sb = new StringBuilder();
		
		listFilesAndFilesSubDirectories(tfRoot.getText());
		
		if (sb.length()==0){
			sb.append("Nenhuma pasta encontrada ! "); 
		}
		
		taResult.setText(sb.toString());
	}
	
	public void listFilesAndFilesSubDirectories(String directoryName){

		File directory = new File(directoryName);

		//get all the files from a directory
		File[] fList = directory.listFiles();

		for (File file : fList){
			
			if (file.isDirectory()){
				if (file.getAbsolutePath().endsWith(tfName.getText())){
					sb.append("rd " + file.getAbsolutePath() + "/s /q \n" );
				} else {
					listFilesAndFilesSubDirectories(file.getAbsolutePath());
				}
				
			}
			
			
			/*
			if (file.isFile()){
				sb.append("file " + file.getAbsolutePath() + "\n");
				System.out.println(file.getAbsolutePath());
			} else if (file.isDirectory()){
				sb.append("folder " + file.getAbsolutePath() + "\n" );
				listFilesAndFilesSubDirectories(file.getAbsolutePath());
			}
			*/
		}
	}	

}
