package br.com.vf.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

//import org.controlsfx.control.action.Action;
//import org.controlsfx.dialog.Dialogs;

import br.com.vf.model.dto.Funcionario;
import br.com.vf.model.util.Utils;
import br.com.vf.view.FuncPane;
import br.com.vf.view.ScreenPane;
import br.com.vf.view.ScreenPaneType;

public class Jm105FuncListaController extends BaseController implements Initializable {
	
	private ObservableList<Funcionario> funcionarioData = FXCollections.observableArrayList();

	@FXML
	private TableView<Funcionario> tabFuncionarios;
	
	@FXML
	private TableColumn<Funcionario, Integer> colCodigo;

	@FXML
	private TableColumn<Funcionario, String> colNome;

	@FXML
	private TableColumn<Funcionario, String> colCargo;
	
	@FXML
	private TableColumn<Funcionario, Double> colSalario;

	@FXML
	private TableColumn<Funcionario, Integer> colSexo;

	@FXML
	private TableColumn<Funcionario, Integer> colSituacao;
	
	@FXML
	private TableColumn<Funcionario, Date> colDtNascimento;
	
	
	@FXML
	private TilePane tpFuncionarios ;
	
	
	@FXML
	private void handleActionBackMenuMetro(ActionEvent event) {
		myScreenPane.setScreen(ScreenPaneType.MENU_METRO);
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		try {
			createFuncionarios();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		colCodigo.setCellValueFactory(new PropertyValueFactory<Funcionario, Integer>("codigo"));
		colNome.setCellValueFactory(new PropertyValueFactory<Funcionario, String>("nome"));
		colCargo.setCellValueFactory(new PropertyValueFactory<Funcionario, String>("cargo"));
		colSalario.setCellValueFactory(new PropertyValueFactory<Funcionario, Double>("salario"));
		colSexo.setCellValueFactory(new PropertyValueFactory<Funcionario, Integer>("sexo"));
		colSituacao.setCellValueFactory(new PropertyValueFactory<Funcionario, Integer>("situacao"));
		colDtNascimento.setCellValueFactory(new PropertyValueFactory<Funcionario, Date>("dataNascimento"));
		
		colCodigo.setCellFactory(new Callback<TableColumn<Funcionario,Integer>, TableCell<Funcionario,Integer>>() {
			@Override
			public TableCell<Funcionario, Integer> call(TableColumn<Funcionario, Integer> param) {
				TableCell<Funcionario, Integer> cell = new TableCell<Funcionario, Integer>(){
					@Override
					protected void updateItem(Integer item, boolean empty) {
						if (item == getItem()) {
		                    return;
		                }
		                super.updateItem( item, empty);
		                if (item == null) {
		                    super.setText(null);
		                    super.setGraphic(null);
		                } else {
		                    super.setText(item.toString());
		                    super.setGraphic(null);
		                }	
					}
				};
				//cell.setTextAlignment(TextAlignment.RIGHT);
				cell.setAlignment(Pos.CENTER);
				return cell;
			}
		});

		colSalario.setCellFactory(new Callback<TableColumn<Funcionario,Double>, TableCell<Funcionario,Double>>() {
			@Override
			public TableCell<Funcionario, Double> call(TableColumn<Funcionario, Double> param) {
				TableCell<Funcionario, Double> cell = new TableCell<Funcionario, Double>(){
					@Override
					protected void updateItem(Double item, boolean empty) {
						if (item == getItem()) {
		                    return;
		                }
		                super.updateItem( item, empty);
		                if (item == null) {
		                    super.setText(null);
		                    super.setGraphic(null);
		                } else {
		                    super.setText(Utils.formatarDouble(item));
		                    super.setGraphic(null);
		                }	
					}
				};
				//cell.setTextAlignment(TextAlignment.RIGHT);
				cell.setAlignment(Pos.CENTER_RIGHT);
				return cell;
			
			}
		});

		colSexo.setCellFactory(new Callback<TableColumn<Funcionario,Integer>, TableCell<Funcionario,Integer>>() {
			@Override
			public TableCell<Funcionario, Integer> call(TableColumn<Funcionario, Integer> param) {
				TableCell<Funcionario, Integer> cell = new TableCell<Funcionario, Integer>(){
					@Override
					protected void updateItem(Integer item, boolean empty) {
						if (item == getItem()) {
		                    return;
		                }
		                super.updateItem( item, empty);
		                if (item == null) {
		                    super.setText(null);
		                    super.setGraphic(null);
		                } else {
		                	if (item.intValue()==1){
		                		super.setText("Masculino");
		                	} else if (item.intValue()==2){
		                		super.setText("Feminino");
		                	} else {
		                		super.setText("");
		                	}
		                    
		                    super.setGraphic(null);
		                }	
					}
				};
				//cell.setTextAlignment(TextAlignment.RIGHT);
				cell.setAlignment(Pos.CENTER_LEFT);
				return cell;
			}
		});

		// http://blog.ngopal.com.np/2011/10/01/tableview-cell-modifiy-in-javafx/
		colSituacao.setCellFactory(new Callback<TableColumn<Funcionario,Integer>, TableCell<Funcionario,Integer>>() {
			@Override
			public TableCell<Funcionario, Integer> call(TableColumn<Funcionario, Integer> param) {
				TableCell<Funcionario, Integer> cell = new TableCell<Funcionario, Integer>(){
					@Override
					protected void updateItem(Integer item, boolean empty) {
						if (item == getItem()) {
		                    return;
		                }
		                super.updateItem( item, empty);
		                if (item == null) {
		                    super.setText(null);
		                    super.setGraphic(null);
		                } else {
		                	CheckBox check = new CheckBox();
		                	if (item.intValue()==1){
		                		check.selectedProperty().setValue(Boolean.TRUE);
		                	}
		                    super.setGraphic(check);
		                }	
					}
				};
				//cell.setTextAlignment(TextAlignment.RIGHT);
				cell.setAlignment(Pos.CENTER);
				cell.setEditable(false);
				return cell;
			}
		});
	
		
		colDtNascimento.setCellFactory(new Callback<TableColumn<Funcionario,Date>, TableCell<Funcionario,Date>>() {
			@Override
			public TableCell<Funcionario, Date> call(TableColumn<Funcionario, Date> param) {
				TableCell<Funcionario, Date> cell = new TableCell<Funcionario, Date>(){
					@Override
					protected void updateItem(Date item, boolean empty) {
						if (item == getItem()) {
		                    return;
		                }
		                super.updateItem( item, empty);
		                if (item == null) {
		                    super.setText(null);
		                    super.setGraphic(null);
		                } else {
		                	super.setText(Utils.formatarData(item));
		                    super.setGraphic(null);
		                }	
					}
				};
				//cell.setTextAlignment(TextAlignment.RIGHT);
				cell.setAlignment(Pos.CENTER);
				return cell;
			}
		});
		
		tabFuncionarios.setItems(funcionarioData);
		
	}
	
	private void createFuncionarios() throws Exception{
		
		Funcionario f01 = new Funcionario();
		f01.setCodigo(1);
		f01.setNome("Dalai Lama");
		f01.setCargo("Monge");
		f01.setSalario(100);
		f01.setFoto("dalailama.jpg");
		f01.setSexo(1);
		f01.setSituacao(1);
		f01.setDataNascimento(Utils.parseDMYToDate("17/01/1943"));
		addFunc(f01);
		
		Funcionario f02 = new Funcionario();
		f02.setCodigo(2);
		f02.setNome("Debora Secco");
		f02.setCargo("Atriz");
		f02.setSalario(10000);
		f02.setFoto("deborasecco.jpg");
		f02.setSexo(2);
		f02.setSituacao(1);
		f02.setDataNascimento(Utils.parseDMYToDate("14/07/1980"));
		addFunc(f02);
	
		Funcionario f03 = new Funcionario();
		f03.setCodigo(3);
		f03.setNome("Gisele Bunchent");
		f03.setCargo("Modelo");
		f03.setSalario(1000000);
		f03.setFoto("giselebunchent.jpg");
		f03.setSexo(2);
		f03.setSituacao(1);
		f03.setDataNascimento(Utils.parseDMYToDate("22/09/1983"));
		addFunc(f03);

		Funcionario f04 = new Funcionario();
		f04.setCodigo(4);
		f04.setNome("Julia Roberts");
		f04.setCargo("Atriz");
		f04.setSalario(500000);
		f04.setFoto("juliaroberts.jpg");
		f04.setSexo(2);
		f04.setSituacao(1);
		f04.setDataNascimento(Utils.parseDMYToDate("04/04/1968"));
		addFunc(f04);
		
		Funcionario f05 = new Funcionario();
		f05.setCodigo(5);
		f05.setNome("Osama Bin Laden");
		f05.setCargo("Terrorista");
		f05.setSalario(420);
		f05.setFoto("osamabinladen.jpg");
		f05.setSexo(1);
		f05.setSituacao(0);
		f05.setDataNascimento(Utils.parseDMYToDate("31/11/1950"));
		addFunc(f05);

		Funcionario f06 = new Funcionario();
		f06.setCodigo(6);
		f06.setNome("Pamela Anderson");
		f06.setCargo("Salva-Vidas");
		f06.setSalario(250000);
		f06.setFoto("pamelaanderson.jpg");
		f06.setSexo(2);
		f06.setSituacao(0);
		f06.setDataNascimento(Utils.parseDMYToDate("05/11/1966"));
		addFunc(f06);

		Funcionario f07 = new Funcionario();
		f07.setCodigo(7);
		f07.setNome("Jenna Jameson");
		f07.setCargo("Stripper");
		f07.setSalario(70000);
		f07.setFoto("jenna-jameson.jpg");
		f07.setSexo(2);
		f07.setSituacao(1);
		f07.setDataNascimento(Utils.parseDMYToDate("17/01/1943"));
		addFunc(f07);
		
		for (int i = 8; i <= 20; i++) {
			f03 = new Funcionario();
			f03.setCodigo(i);
			f03.setNome("Jenna Jameson");
			f03.setCargo("Babysitter");
			f03.setSalario(i);
			if (i%2==0){
				f03.setFoto("jenna-jameson.jpg");	
			} 
			f03.setSexo(1);
			f03.setSituacao(1);
			f03.setDataNascimento(Utils.parseDMYToDate("17/01/1943"));
			addFunc(f03);
		}
		
	}
	
	private void addFunc(Funcionario funcionario){
		funcionarioData.add(funcionario);
		FuncPane funcPane = new FuncPane(funcionario);
		tpFuncionarios.getChildren().add(funcPane);
	}
	
	@FXML
	private void handleActionAddFunc(ActionEvent event) {
		Funcionario func = new Funcionario();
		if (openStageFuncCad(func)){
			addFunc(func);
		}
	}
	
	@FXML
	private void handleActionEditFunc(ActionEvent event) {
		Funcionario selectedFunc = tabFuncionarios.getSelectionModel().getSelectedItem();
		if (selectedFunc != null){
			if (openStageFuncCad(selectedFunc)){
				refreshTable();
			}
		} else {
			
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("No Selection");
			alert.setHeaderText("No Person Selected");
			alert.setContentText("Please select a person in the table.");
			alert.showAndWait(); 

			//Dialogs.create().owner(myScreenPane.getPrimaryStage()).title("No Selection").masthead("No Person Selected").message("Please select a person in the table.").showWarning();
		}
	}
	
	@FXML
	private void handleActionRemoveFunc(ActionEvent event) {
		
		int selectedIndex = tabFuncionarios.getSelectionModel().getSelectedIndex();
		if (selectedIndex >= 0) {
			//Action response = Dialogs.create().owner(myScreenPane.getPrimaryStage()).title(ScreenPane.APP_TITLE).masthead(ScreenPane.DLG_CONFIRM).message("Confirma a exclusao deste registro?").actions(org.controlsfx.dialog.Dialog.Actions.YES, org.controlsfx.dialog.Dialog.Actions.NO).showConfirm();
			//if (response == org.controlsfx.dialog.Dialog.Actions.YES){
			//	delFunc(selectedIndex);
			//}
			
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle(ScreenPane.APP_TITLE);
			alert.setHeaderText(ScreenPane.DLG_CONFIRM);
			alert.setContentText("Confirma a exclusao deste registro?");

			Optional<ButtonType> result = alert.showAndWait();
			
			if (result.get() == ButtonType.OK){
				delFunc(selectedIndex);
			}
			
		} else {
			// Nothing selected
			
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("No Selection");
			alert.setHeaderText("No Person Selected");
			alert.setContentText("Please select a person in the table.");
			alert.showAndWait(); 

			
			//"//Dialogs.create().owner(myScreenPane.getPrimaryStage()).title("No Selection").masthead("No Person Selected").message("Please select a person in the table.").showWarning();
		}	
	}
	
	private void delFunc(int index){
		
		FuncPane funcPane = null;
		
		int idFunci = tabFuncionarios.getSelectionModel().getSelectedItem().getCodigo();
		
		for (int i = 0; i < tpFuncionarios.getChildren().size(); i++) {
			funcPane = (FuncPane)tpFuncionarios.getChildren().get(i);
			
			if (idFunci==funcPane.getFuncionario().getCodigo()){
				tpFuncionarios.getChildren().remove(i);
			}
		}
		
		tabFuncionarios.getItems().remove(index);

		
	}
	
	private void refreshTable() {
		
		FuncPane funcPane = null;
		
		
		int selectedIndex = tabFuncionarios.getSelectionModel().getSelectedIndex();
		tabFuncionarios.setItems(null);
		tabFuncionarios.layout();
		tabFuncionarios.setItems(funcionarioData);
		// Must set the selected index again (see http://javafx-jira.kenai.com/browse/RT-26291)
		tabFuncionarios.getSelectionModel().select(selectedIndex);
		
		Funcionario func = tabFuncionarios.getSelectionModel().getSelectedItem();
		
		for (int i = 0; i < tpFuncionarios.getChildren().size(); i++) {
			funcPane = (FuncPane)tpFuncionarios.getChildren().get(i);
			if (func.getCodigo()==funcPane.getFuncionario().getCodigo()){
				funcPane.refreshFuncPane(func);
			}
		}
	}	
	
	private boolean openStageFuncCad(Funcionario funcionario){
		
		try {
			// Load the fxml file and create a new stage for the popup
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/vf/view/Jm105FuncCad.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Funcionário");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.setResizable(false);
			dialogStage.initOwner(myScreenPane.getPrimaryStage());
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			
			// Set the persons into the controller
			Jm105FuncCadController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setFuncionario(funcionario);
			
			dialogStage.showAndWait();
			
			return controller.isOkClicked();
			
		} catch (IOException e) {
			// Exception gets thrown if the fxml file could not be loaded
			e.printStackTrace();
			return false;
		}	
		
		
		
	}
	
	

}
