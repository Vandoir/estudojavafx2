package br.com.vf.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import br.com.vf.view.ScreenPane;
import br.com.vf.view.ScreenPaneType;

public class TemplateController extends BaseController implements Initializable {

	@FXML
	private Label lbXX;
	
	@FXML
	private TextField tfXX;
	
	@FXML
	private void handleActionBackMenuMetro(ActionEvent event) {
		myScreenPane.setScreen(ScreenPaneType.MENU_METRO);
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// initialize
	}
	
	@FXML
	private void onClickLabelXX(MouseEvent event) {
		if (event.getClickCount()==2){
			tfXX.setText("onClick");
		}
	}

	@FXML
	private void handleActionOk(ActionEvent event) {
		if (isInputValid()){
			tfXX.setText("Ok");
		}
	}
	
	private boolean isInputValid() {
		String errorMessage = "";
		
		if (tfXX.getText() == null || tfXX.getText().length() == 0) {
			errorMessage += "Pasta 1 � um campo obrigat�rio!\n"; 
		}

		if (tfXX.getText() == null || tfXX.getText().length() == 0) {
			errorMessage += "Pasta 2 � um campo obrigat�rio!\n"; 
		}
		
		if (errorMessage.length() == 0) {
			return true;
		} else {
			// Show the error message
			
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle(ScreenPane.DLG_INVALID_FIELD_TITLE);
			alert.setHeaderText(ScreenPane.DLG_INVALID_FIELD_HEAD);
			alert.setContentText(errorMessage);
			alert.showAndWait(); 

			
			//org.controlsfx.dialog.Dialogs.create().owner(myScreenPane.getPrimaryStage()).title(ScreenPane.DLG_INVALID_FIELD_TITLE).message(errorMessage).masthead(ScreenPane.DLG_INVALID_FIELD_HEAD).showError();
			return false;
		}
	}
	

}
