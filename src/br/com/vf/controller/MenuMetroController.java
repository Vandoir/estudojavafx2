package br.com.vf.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import br.com.vf.view.ScreenPane;
import br.com.vf.view.ScreenPaneType;

public class MenuMetroController extends BaseController implements Initializable{
	
	private StringProperty conexaoValue = new SimpleStringProperty("Conex�o : [ ]");
	
    @FXML
    private Text statusText;

    @FXML
    private Label lblConexao;
    
    @FXML
    private ImageView imgCalc;

    @FXML
    private ImageView imgPdf;

    @FXML
    private ImageView imgLock;

    @FXML
    private ImageView imgAndroid;

    @FXML
    private ImageView imgHelp;
    
    @FXML
    private ImageView imgAngry;
    
    @FXML
    private ImageView imgLive;

    @FXML
    private ImageView imgSearch;

    @FXML
    private ImageView imgTask;

    @FXML
    private ImageView imgRecycle;
    
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		statusText.setText("");
		lblConexao.textProperty().bind(conexaoValue);
	}

	@Override
	public void setScreenPane(ScreenPane screenPage) {
		myScreenPane = screenPage;
		if (myScreenPane.getLoggedUser()!=null){
			statusText.setText("Usuario logado :"+ myScreenPane.getLoggedUser().getUser() );
		} else {
			statusText.setText("Usuario logado = null");
		}
	}
    
	@Override
	public void refreshScreen() {
		System.out.println("menu refresh");
	}

	public void imageViewEnter(MouseEvent event){
		//if((Button)e.getSource() == checkBtn){
		ImageView imgView = (ImageView)event.getSource() ;
		imgView.setScaleX(0.97);
		imgView.setScaleY(0.97);
		imgView.setCursor(Cursor.HAND);
		
		if (imgView==imgCalc){
			statusText.setText("Voltar ao Login" );
		} else if (imgView==imgPdf){ 
			statusText.setText("Pdf - Despesas");
		} else if (imgView==imgLock){ 
			statusText.setText("Lock - Folder Remove");
		} else if (imgView==imgAndroid){ 
			statusText.setText("Android - Funcionarios");
		} else if (imgView==imgHelp){ 
			statusText.setText("Help - Dicas");
		} else if (imgView==imgAngry){ 
			statusText.setText("Angry - Folder Compare");
		} else if (imgView==imgLive){ 
			statusText.setText("Live - DB Explorer");
		} else if (imgView==imgSearch){ 
			statusText.setText("* Search *");
		} else if (imgView==imgTask){ 
			statusText.setText("Video - WebCam");
		} else if (imgView==imgRecycle){ 
			statusText.setText("Recycle - Template");
		}
		
	}
	
	public void imageViewExit(MouseEvent event){
		ImageView imgView = (ImageView)event.getSource() ;
		imgView.setScaleX(1.0);
		imgView.setScaleY(1.0);
		statusText.setText("");
	}
	
	@FXML
	private void imageViewClicked(MouseEvent event) {
		ImageView imgView = (ImageView)event.getSource() ;

		if (imgView==imgCalc){
			myScreenPane.setScreen(ScreenPaneType.LOGIN);
		} else if (imgView==imgPdf){ 
			myScreenPane.setScreen(ScreenPaneType.JM105_DESPESAS);
		} else if (imgView==imgLock){ 
			myScreenPane.setScreen(ScreenPaneType.FOLDER_REMOVE);
		} else if (imgView==imgAndroid){ 
			myScreenPane.setScreen(ScreenPaneType.JM105_FUNC_LISTA);
		} else if (imgView==imgHelp){ 
			myScreenPane.setScreen(ScreenPaneType.DICAS);
		} else if (imgView==imgAngry){ 
			myScreenPane.setScreen(ScreenPaneType.FOLDER_COMPARE);
		} else if (imgView==imgLive){ 
			myScreenPane.setScreen(ScreenPaneType.DB_EXPLORER);
		} else if (imgView==imgSearch){ 
			statusText.setText("imageViewClicked Search");
		} else if (imgView==imgTask){ 
			myScreenPane.setScreen(ScreenPaneType.WEB_CAM);
		} else if (imgView==imgRecycle){ 
			myScreenPane.setScreen(ScreenPaneType.TEMPLATE);
		}
		
	}
	
	
	@FXML
	private void handleActionConecta() {
		boolean okClicked = openStageConexao();
		if (okClicked) {
			conexaoValue.setValue("Conex�o : Conectado") ;
		}
	}
	
	
	
	@FXML
	private void handleActionDesconecta() {
		conexaoValue.setValue("Conex�o : [ ]");
	}

	private boolean openStageConexao(){
		
		try {
			// Load the fxml file and create a new stage for the popup
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/ConexaoJdbc.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Conex�o");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(myScreenPane.getPrimaryStage());
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			
			// Set the persons into the controller
			ConexaoJdbcController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			
			dialogStage.showAndWait();
			
			return controller.isOkClicked();
			
		} catch (IOException e) {
			// Exception gets thrown if the fxml file could not be loaded
			e.printStackTrace();
			return false;
		}	
		
		
		
	}
	
	

}
