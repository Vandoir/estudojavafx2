package br.com.vf.controller;

import java.net.URL;
import java.util.Enumeration;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import br.com.vf.view.ScreenPaneType;

public class DicasController extends BaseController implements Initializable {

	@FXML
	private WebView wvDicas;
	
	@FXML
	private void handleActionBackMenuMetro(ActionEvent event) {
		myScreenPane.setScreen(ScreenPaneType.MENU_METRO);
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		loadPaginaDicas();
	}
	
//	@FXML
//	private void onClickTitleLabel(MouseEvent event) {
//		if (event.getClickCount()==2){
//			loadPaginaDicas();
//		}
//	}
	
	private void loadPaginaDicas(){
		try {
			WebEngine webEngine = wvDicas.getEngine();
			URL urlDicas = getClass().getResource("/br/com/vf/view/Dicas.html");
		    webEngine.load(urlDicas.toExternalForm());
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@FXML
	private void handleActionShowDicas() {
		loadPaginaDicas();
	}

	@FXML
	private void handleActionShowProperties() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("<html>");
		sb.append("<head>");
		sb.append("<style>");
		sb.append("table");
		sb.append("{");
		//sb.append("width:300px;");
		sb.append("border:1px solid #c3c3c3;");
		sb.append("border-collapse:collapse;");		
		sb.append("}");
		sb.append("th");
		sb.append("{");
		sb.append("background-color:#C3D9FF;");
		sb.append("border:1px solid #c3c3c3;");		
		sb.append("padding:3px;");
		sb.append("}");
		sb.append("td");
		sb.append("{");
		sb.append("border:1px solid #c3c3c3;");
		sb.append("padding:3px;");
		sb.append("}");
		
		sb.append("</style>");
		sb.append("</head>");
		sb.append("<body>");
		
		sb.append("<h2>Examining System Properties</h2>");
		sb.append("<table border='1' cellspacing='0' cellpadding='0' >");
		sb.append("<tr>");
		sb.append("<th>Nome</th>");
		sb.append("<th>Valor</th>");
		sb.append("</tr>");
		String nome = ""; 
		Enumeration<?> enuProp = System.getProperties().propertyNames();
		while (enuProp.hasMoreElements()) {
			nome = (String) enuProp.nextElement();
			sb.append("<TR><TD>" + nome+"</TD><TD>" + System.getProperty(nome).toString() + "</TR>" );
		}
		sb.append("</table>");

		sb.append("<h2>System.getProperties()</h2>");
		sb.append("<p>Properties properties = System.getProperties();<br>properties.list(System.out); </p>");

		sb.append("</body>");
		sb.append("</html>");
		
		wvDicas.getEngine().loadContent(sb.toString());

		
	}
	

}
