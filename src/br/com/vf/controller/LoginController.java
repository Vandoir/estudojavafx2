package br.com.vf.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import br.com.vf.model.bo.AuthenticatorBo;
import br.com.vf.view.ScreenPaneType;

public class LoginController extends BaseController implements Initializable{
	
    @FXML
    private Label lbUser;
    
    @FXML
    private Label lbPassword;

    @FXML
    private TextField tfUser;
    
    @FXML
    private PasswordField pfPassword;

    @FXML
    private Button btnEntrar;
    
    @FXML
    private void handleActionEntrar(ActionEvent event) {
    	this.processLogin();
    }

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		
		lbUser.setText("");
		lbPassword.setText("");
		
		//tfUser.setText("admin");
		//pfPassword.setText("admin");
		
		Platform.runLater(new Runnable() {
	        @Override
	        public void run() {
	        	btnEntrar.requestFocus();
	        }
	    });		
		
	}

	private boolean processLogin(){
		boolean retorno = true;

		lbUser.setText("");
		lbPassword.setText("");
		
		if ("".equals(tfUser.getText())){
			lbUser.setText("Digite o usu�rio");
			retorno = false;
		}

		if ("".equals(pfPassword.getText())){
			lbPassword.setText("Digite a senha");
			retorno = false;
		}
		
		if (retorno){
			
			if (AuthenticatorBo.validate(tfUser.getText(), pfPassword.getText())) {
	            myScreenPane.setLoggedUser(AuthenticatorBo.getUser(tfUser.getText()));
	            myScreenPane.setScreen(ScreenPaneType.MENU_METRO);
	            return true;
	        } else {
				lbUser.setText("Usu�rio ou senha inv�lidos");
				retorno = false;
			}
			
		}
		
		return retorno;
	}

}
