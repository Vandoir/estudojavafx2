package br.com.vf.view;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import br.com.vf.model.dto.Funcionario;

public class FuncPane extends AnchorPane {
	
	private Funcionario funcionario = null ; 
	
	private Label lblCod = new Label();
	private Label lblNome = new Label();
	private Label lblCargo = new Label();
	private ImageView imageView = new ImageView();
	
	public Funcionario getFuncionario() {
		return funcionario;
	}

	public FuncPane(Funcionario func) {
		this.init();
		refreshFuncPane(func);
	}
	
	
	public void refreshFuncPane(Funcionario func){
		this.funcionario = func;
		lblCod.setText(String.valueOf(funcionario.getCodigo()));
		lblNome.setText(funcionario.getNome());
		lblCargo.setText(funcionario.getCargo());
		
		Image imagem = null ; 
		try {
			imagem = new Image(getClass().getResourceAsStream("/br/com/vf/assets/func/"+funcionario.getFoto()));
		} catch (Exception e) {
			imagem = new Image(getClass().getResourceAsStream("/br/com/vf/assets/func/no-photo.jpg"));
		}	
		
		imageView.setImage(imagem);
		
	}
	
	private void init(){	
		
		this.setStyle("-fx-border-color: #666666; -fx-border-width: 1;");
		
		Label lbCod = new Label();
		lbCod.setText("Codigo");
		lbCod.setLayoutX(10.0);
		lbCod.setLayoutY(156.0);
		this.getChildren().add(lbCod);
		
		//Label lblCod = new Label();
		
		lblCod.setStyle("-fx-font-weight: bold;");
		lblCod.setLayoutX(55.0);
		lblCod.setLayoutY(156.0);
		this.getChildren().add(lblCod);

		Label lbNome = new Label();
		lbNome.setText("Nome");
		lbNome.setLayoutX(10.0);
		lbNome.setLayoutY(170.0);
		this.getChildren().add(lbNome);

		//Label lblNome = new Label();
		
		lblNome.setLayoutX(10.0);
		lblNome.setLayoutY(184.0);
		lblNome.setStyle("-fx-font-weight: bold;");
		this.getChildren().add(lblNome);

		Label lbCargo = new Label();
		lbCargo.setText("Cargo");
		lbCargo.setLayoutX(10.0);
		lbCargo.setLayoutY(198.0);
		this.getChildren().add(lbCargo);

		//Label lblCargo = new Label();
		
		lblCargo.setLayoutX(10.0);
		lblCargo.setLayoutY(212.0);
		lblCargo.setStyle("-fx-font-weight: bold;");
		this.getChildren().add(lblCargo);
		
		imageView.setLayoutX(8.0);
		imageView.setLayoutY(8.0);
		this.getChildren().add(imageView);
		
		
	}

}
