package br.com.vf.view;

public enum ScreenPaneType {
	
	LOGIN("login","Login.fxml"),
	DICAS("dicas","Dicas.fxml"),
	FOLDER_COMPARE("folderCompare","FolderCompare.fxml"),
	FOLDER_REMOVE("folderRemove","FolderRemove.fxml"),
	MENU_METRO("menuMetro","MenuMetro.fxml"),
	JM105_DESPESAS("jm105","Jm105Despesas.fxml"),
	JM105_FUNC_LISTA("jm105Func","Jm105FuncLista.fxml"),
	WEB_CAM("webcam","WebCam.fxml"),
	DB_EXPLORER("dbexplorer","DbExplorer.fxml"),
	TEMPLATE("template","Template.fxml");
	
	private String name;
	private String resource;
	
	private ScreenPaneType(String name, String resource) {
		this.name = name;
		this.resource = resource;
	}

	public String getName() {
		return name;
	}

	public String getResource() {
		return resource;
	}

}
