package br.com.vf.view;

import java.util.HashMap;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import br.com.vf.model.dto.User;

public class ScreenPane extends StackPane {
	
	public static final String APP_TITLE = "Estudos de JavaFX2" ;
	public static final String APP_VERSION = "1.0.0.6 11/02/2017" ;
	public static final String DLG_CONFIRM = "Dialogo de Confirmação";
	public static final String DLG_WARNING = "Dialogo de Warning";
	public static final String DLG_ERROR_EXCEPTION = "Dialogo de Erro com Exception";
	public static final String DLG_INVALID_FIELD_TITLE = "Campos Inválidos";
	public static final String DLG_INVALID_FIELD_HEAD = "Favor corrigir os campos inválidos";
	
    //Holds the screens to be displayed
    private HashMap<String, Node> screens = new HashMap<>();
    private Stage primaryStage = null ; 
    private double DOIS = 200; // era 2000
    private double UM = 100;   // era 1000
    
    private User loggedUser = null; 
    
    public ScreenPane() {
        super();
    }
    
    public Stage getPrimaryStage() {
		return primaryStage;
	}


	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	//Add the screen to the collection
    public void addScreen(String name, Node screen){
        screens.put(name, screen);
    }
    
    //Returns the Node with the appropriate name
    public Node getScreen(String name){
        return screens.get(name);
    }
    
    public User getLoggedUser() {
		return loggedUser;
	}

	public void setLoggedUser(User loggedUser) {
		this.loggedUser = loggedUser;
	}


	//Loads the fxml file, add the screen to the screens collection and
    //finally injects the screenPane to the controller.
	//public boolean loadScreen(String name, String resource){
    public boolean loadScreen(ScreenPaneType screenPaneType){
        try{
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource(screenPaneType.getResource()));
            Parent loadScreen = (Parent) myLoader.load();
            //loadScreen.setBlendMode(arg0) Style(arg0)(getClass().getResource("Style.css").toExternalForm());
            //loadScreen.getStylesheets().add("/Style.css");
            loadScreen.getStylesheets().add(ScreenPane.class.getResource("Style.css").toExternalForm());
            ScreenController myScreenControler = ((ScreenController)myLoader.getController());
            myScreenControler.setScreenPane(this);
            addScreen(screenPaneType.getName(), loadScreen);
            return true;
        }catch(Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }
    
    //This method tries to displayed the screen with a predefined name.
    //First it makes sure the screen has been already loaded.  Then if there is more than
    //one screen the new screen is been added second, and then the current screen is removed.
    // If there isn't any screen being displayed, the new screen is just added to the root.
    
   // public boolean setScreen(final String name){
    public boolean setScreen(final ScreenPaneType screenPaneType){
    	if (screens.get(screenPaneType.getName()) != null) {   //screen loaded
    		final DoubleProperty opacity = opacityProperty();

    		if (!getChildren().isEmpty()) {    //if there is more than one screen
    			Timeline fade = new Timeline(
    					new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
    					new KeyFrame(new Duration(DOIS), new EventHandler<ActionEvent>() {
    						@Override
    						public void handle(ActionEvent t) {
    							getChildren().remove(0);                    //remove the displayed screen
    							getChildren().add(0, screens.get(screenPaneType.getName()));     //add the screen
    							Timeline fadeIn = new Timeline(
    									new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
    									new KeyFrame(new Duration(DOIS), new KeyValue(opacity, 1.0)));
    							fadeIn.play();
    						}
    					}, new KeyValue(opacity, 0.0)));
    			fade.play();
    			

    		} else {
    			setOpacity(0.0);
    			getChildren().add(screens.get(screenPaneType.getName()));       //no one else been displayed, then just show

    			Timeline fadeIn = new Timeline(
    					new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
    					new KeyFrame(new Duration(UM), new KeyValue(opacity, 1.0)));
    			fadeIn.play();
    		}
    		return true;
    	} else {
    		System.out.println("screen hasn't been loaded!!! \n");
    		return false;
    	}

        
        /*Node screenToRemove;
        if(screens.get(name) != null){   //screen loaded
            if(!getChildren().isEmpty()){    //if there is more than one screen
                getChildren().add(0, screens.get(name));     //add the screen
                screenToRemove = getChildren().get(1);
                getChildren().remove(1);                    //remove the displayed screen
            }else{
             getChildren().add(screens.get(name));       //no one else been displayed, then just show
            }
            return true;
        }else {
            System.out.println("screen hasn't been loaded!!! \n");
            return false;
        }*/
    }
    
    
    //This method will remove the screen with the given name from the collection of screens
    public boolean unloadScreen(String name){
        if(screens.remove(name) == null)
        {    
            System.out.println("Screen didn't exist");
            return false;
        }else{return true;}
    }

}
