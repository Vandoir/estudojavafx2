package br.com.vf.view;

public interface ScreenController {
	
    //This method will allow the injection of the ScreenPane
    public void setScreenPane(ScreenPane screenPage);
    public void refreshScreen();
    

}
